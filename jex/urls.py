"""jex URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from JExam.api import ParticipantResource, CourseResource, AssessmentResource, ChoiceResource, QuestionResource, \
    SessionResource, AnswerResource, SolutionResource, UserResource
from django.conf.urls import url, include
from django.contrib import admin
from tastypie.api import Api

jexam_api = Api(api_name="v1")
jexam_api.register(UserResource())
jexam_api.register(ParticipantResource())
jexam_api.register(CourseResource())
jexam_api.register(AssessmentResource())
jexam_api.register(ChoiceResource())
jexam_api.register(QuestionResource())
jexam_api.register(SessionResource())
jexam_api.register(AnswerResource())
jexam_api.register(SolutionResource())

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^api/', include(jexam_api.urls)),
]
