from JExam.fake import OAuth20Authentication, UserObjectsOnlyAuthorization
from JExam.models import Question, Course, Choice, Assessment, Answer, Participant, Session, Solution
from django.contrib.auth.models import User
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        authentication = OAuth20Authentication()
        authorization = UserObjectsOnlyAuthorization()
        excludes = ['resource_uri', 'date_joined', 'password', 'is_staff', 'is_superuser']
        filtering = {
            'username': ALL,
        }

    def hydrate(self, bundle):
        if 'raw_password' in bundle.data:
            u = User(username='dummy')
            u.set_password(bundle.data['raw_password'])
            bundle.data['password'] = u.password
        return bundle

    def dehydrate(self, bundle):
        # del bundle.data['password']
        return bundle


class CourseResource(ModelResource):
    class Meta:
        queryset = Course.objects.all()
        resource_name = 'course'
        authentication = OAuth20Authentication()
        authorization = UserObjectsOnlyAuthorization()
        filtering = {
            'name': ALL
        }
        ordering = ['name']


class ParticipantResource(ModelResource):
    user = fields.ForeignKey(UserResource, 'user', full=True)
    course = fields.ForeignKey('JExam.api.CourseResource', 'course', full=True)

    class Meta:
        queryset = Participant.objects.all()
        resource_name = 'participant'
        authentication = OAuth20Authentication()
        authorization = UserObjectsOnlyAuthorization()
        filtering = {
            'user': ALL_WITH_RELATIONS,
            'course': ALL_WITH_RELATIONS,
        }
        ordering = ['user.username']


class AssessmentResource(ModelResource):
    course = fields.ForeignKey('JExam.api.CourseResource', 'course', full=True)

    class Meta:
        queryset = Assessment.objects.all()
        resource_name = 'assessment'
        authentication = OAuth20Authentication()
        authorization = UserObjectsOnlyAuthorization()
        filtering = {
            'course': ALL_WITH_RELATIONS,
            'id': ALL
        }


class ChoiceResource(ModelResource):
    question = fields.ForeignKey('JExam.api.QuestionResource', 'question', full=True, null=True, blank=True)
    answer = fields.ForeignKey('JExam.api.AnswerResource', 'answer', full=True, null=True, blank=True)
    solution = fields.ForeignKey('JExam.api.SolutionResource', 'solution', full=True, null=True, blank=True)

    class Meta:
        queryset = Choice.objects.all()
        resource_name = 'choice'
        authentication = OAuth20Authentication()
        authorization = UserObjectsOnlyAuthorization()
        filtering = {
            'question': ALL_WITH_RELATIONS,
            'answer': ALL_WITH_RELATIONS
        }


class QuestionResource(ModelResource):
    assessment = fields.ForeignKey('JExam.api.AssessmentResource', 'assessment', full=True)

    class Meta:
        queryset = Question.objects.all()
        resource_name = 'question'
        authentication = OAuth20Authentication()
        authorization = UserObjectsOnlyAuthorization()
        filtering = {
            'title': ALL,
            'type': ALL,
            'assessment': ALL_WITH_RELATIONS
        }


class SessionResource(ModelResource):
    assessment = fields.ForeignKey('JExam.api.AssessmentResource', 'assessment', full=True)

    class Meta:
        queryset = Session.objects.all()
        resource_name = 'session'
        authentication = OAuth20Authentication()
        authorization = UserObjectsOnlyAuthorization()
        filtering = {
            'assessment': ALL_WITH_RELATIONS,
            'status': ALL
        }


class AnswerResource(ModelResource):
    question = fields.ForeignKey('JExam.api.QuestionResource', 'question', full=True)
    session = fields.ForeignKey('JExam.api.SessionResource', 'session', full=True)

    class Meta:
        queryset = Answer.objects.all()
        resource_name = 'answer'
        authentication = OAuth20Authentication()
        authorization = UserObjectsOnlyAuthorization()
        filtering = {
            'question': ALL_WITH_RELATIONS,
            'session': ALL_WITH_RELATIONS,
            'participant': ALL_WITH_RELATIONS
        }


class SolutionResource(ModelResource):
    question = fields.ForeignKey('JExam.api.QuestionResource', 'question', full=True)
    assessment = fields.ForeignKey('JExam.api.AssessmentResource', 'assessment', full=True)

    class Meta:
        queryset = Solution.objects.all()
        resource_name = 'solution'
        authentication = OAuth20Authentication()
        authorization = UserObjectsOnlyAuthorization()
        filtering = {
            'question': ALL_WITH_RELATIONS,
            'assessment': ALL_WITH_RELATIONS
        }
