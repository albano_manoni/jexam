from django.db import models


class Course(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    description = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return u'Name: %s' % self.name


class Participant(models.Model):
    user = models.ForeignKey('auth.User', related_name='partecipates', null=False, blank=False)
    course = models.ForeignKey('Course', related_name='participants', null=False, blank=False)

    def __str__(self):
        return u'User: %s, Course %s' % (self.user.username, self.course.name)


class Assessment(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    description = models.CharField(max_length=255, null=True, blank=True)
    course = models.ForeignKey('Course', related_name='assessments', null=False, blank=False)
    duration = models.IntegerField(default=30, null=False, blank=False)

    def __str__(self):
        return u'Name: %s' % self.name


class Choice(models.Model):
    question = models.ForeignKey('Question', related_name='choices', null=True, blank=True)
    answer = models.ForeignKey('Answer', related_name='choices', null=True, blank=True)
    solution = models.ForeignKey('Solution', related_name='solutions', null=True, blank=True)
    value = models.CharField(max_length=255, null=False, blank=False)

    def __str__(self):
        return u'Value: %s' % self.value


class Question(models.Model):
    title = models.CharField(max_length=255, null=False, blank=False, default="Question")
    assessment = models.ForeignKey('Assessment', related_name='questions', null=False, blank=False)
    type = models.SmallIntegerField(choices=((0, 'Single'), (1, 'Multiple'), (2, 'Open')), default=0)

    def __str__(self):
        return u'Title:  %s, Assessment: %s' % (self.title, self.assessment.name)


class Session(models.Model):
    assessment = models.ForeignKey('Assessment', related_name='sessions', null=False, blank=False)
    start_time = models.DateTimeField()
    finish_time = models.DateTimeField()
    participant = models.ForeignKey('Participant', related_name='sessions', null=True, blank=True)
    status = models.SmallIntegerField(choices=((0, 'Created'), (1, 'Submitted'), (2, 'Passed'), (3, 'Failed')),
                                      default=0)

    def __str__(self):
        return u'Assessment: %s, Status: %s' % (self.assessment.name, self.status)


class Answer(models.Model):
    question = models.ForeignKey('Question', related_name='answers', null=False, blank=False)
    session = models.ForeignKey('Session', related_name='answers', null=True, blank=True)
    text = models.TextField(null=True, blank=True)

    def __str__(self):
        return u'Question %s' % self.question


class Solution(models.Model):
    assessment = models.ForeignKey('Assessment', null=False, blank=False)
    question = models.ForeignKey('Question', null=True, blank=False)

    def __str__(self):
        return u'Question: %s, Assessment: %s' % (self.question.title, self.assessment.name)
