from django.contrib import admin

from JExam.models import Session, Answer, Assessment, Choice, Course, Question, Participant, Solution


class ParticipantAdmin(admin.ModelAdmin):
    pass


class SessionAdmin(admin.ModelAdmin):
    pass


class AnswerAdmin(admin.ModelAdmin):
    pass


class AssessmentAdmin(admin.ModelAdmin):
    pass


class ChoiceAdmin(admin.ModelAdmin):
    pass


class CourseAdmin(admin.ModelAdmin):
    pass


class QuestionAdmin(admin.ModelAdmin):
    pass


class SolutionAdmin(admin.ModelAdmin):
    pass


admin.site.register(Session, SessionAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(Assessment, AssessmentAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Participant, ParticipantAdmin)
admin.site.register(Solution, SolutionAdmin)
