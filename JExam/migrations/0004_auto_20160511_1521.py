# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-11 15:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('JExam', '0003_question_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='session',
            name='status',
            field=models.SmallIntegerField(choices=[(0, 'Created'), (1, 'Submitted'), (2, 'Passed'), (3, 'Failed')], default=0),
        ),
    ]
